//EMPRESA

function enviarE(id) {
    //Obtención de la información del formulario empresa
    let razonsocial = document.getElementById("txt-razonsocial").value;
    let rol = document.getElementById("txt-rol").value;
    let email2 = document.getElementById("txt-email2").value;
    let telefono3 = document.getElementById("txt-telefono3").value;
    let telefono4 = document.getElementById("txt-telefono4").value;
    let direccion2 = document.getElementById("txt-direccion2").value;
    let comuna2 = document.getElementById("comuna2").value;
    let ciudad2 = document.getElementById("ciudad2").value;
    let password3 = document.getElementById("txt-password3").value;




    //Validaciones de campos
    let message = isEmpty(razonsocial, "RAZÓN SOCIAL");
    message = message + isMaxLength(razonsocial, "RAZÓN SOCIAL", 57);
    message = message + isEmpty(rol, "ROL");
    message = message + isMaxLength(rol, "ROL", 13);
    message = message + isEmpty(email2, "EMAIL");
    message = message + isMaxLength(email2, "EMAIL", 72);
    message = message + validateEmail(email2, "EMAIL");
    message = message + isEmpty(telefono3, "TELÉFONO OBLIGATORIO");
    message = message + isMaxLength(telefono3, "TELÉFONO OBLIGATORIO", 13);
    message = message + validatePhone(telefono3, "TELÉFONO OBLIGATORIO");
    message = message + isEmpty(telefono4, "TELÉFONO NO OBLIGATORIO");
    message = message + isMaxLength(telefono4, "TELÉFONO NO OBLIGATORIO", 13);
    message = message + validatePhone(telefono4, "TELÉFONO NO OBLIGATORIO");
    message = message + isEmpty(direccion2, "DIRECCIÓN");
    message = message + isMaxLength(direccion2, "DIRECCIÓN", 700);
    message = message + isEmpty(comuna2, "COMUNA");
    message = message + isMaxLength(comuna2, "COMUNA", 700);
    message = message + isEmpty(ciudad2, "CIUDAD");
    message = message + isMaxLength(ciudad2, "CIUDAD", 700);
    message = message + isEmpty(password3, "PASSWORD");
    message = message + isMaxLength(password3, "PASSWORD", 20);




    if (message.length > 0) {
        alert("ERROR: \n" + message);
    } else {
        document.getElementById(id).submit();
    }
}

function limpiarE() {

    console.log("EJECUTANDO MÉTODO LIMPIAR.");
    //Validar si los campos existen (undefined o null)
    editElementText("txt-razonsocial", "");
    editElementText("txt-rol", "");
    editElementText("txt-email2", "");
    editElementText("txt-telefono3", "");
    editElementText("txt-telefono4", "");
    editElementText("txt-direccion2", "");
    editElementText("txt-password3", "");
    editElementText("txt-password4", "");

    console.log("FINALIZANDO MÉTODO LIMPIAR.");
}

//OTRAS VALIDACIONES
function editElementText(elementName, value) {

    let element = document.getElementById(elementName);

    if (element != null) {
        element.value = value;
    } else {
        console.warn("El elemento " + elementName + " no se encuentra en el documento HTML");
    }
}

function validatePhone(phone, name) {
    let value = phone
    if (/^([0-9])*$/g.test(value)) {
        return "";
    } else {
        return "El campo " + name + " debe ser númerico.\n";
    }
}

function validateEmail(email, name) {

    let value = email
    if (/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(value)) {
        return "";
    } else {
        return "El campo " + name + " no es un correo electrónico válido\n";
    }
}