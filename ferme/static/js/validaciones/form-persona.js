//PERSONA

function enviar(id) {
    //Obtención de la información del formulario persona
    let nombres = document.getElementById("txt-nombres").value;
    let apellidoPaterno = document.getElementById("txt-apellido-paterno").value;
    let apellidoMaterno = document.getElementById("txt-apellido-materno").value;
    let rut = document.getElementById("txt-rut").value;
    let email1 = document.getElementById("txt-email1").value;
    let telefono1 = document.getElementById("txt-telefono1").value;
    let telefono2 = document.getElementById("txt-telefono2").value;
    let direccion1 = document.getElementById("txt-direccion1").value;
    let password1 = document.getElementById("txt-password1").value;




    //Validaciones de campos
    let message = isEmpty(nombres, "NOMBRES");
    message = message + isMaxLength(nombres, "NOMBRES", 57);
    message = message + isEmpty(apellidoPaterno, "APELLIDO PATERNO");
    message = message + isMaxLength(apellidoPaterno, "APELLIDO PATERNO", 82);
    message = message + isEmpty(apellidoMaterno, "APELLIDO MATERNO");
    message = message + isMaxLength(apellidoMaterno, "APELLIDO MATERNO", 82);
    message = message + isEmpty(rut, "RUT");
    message = message + isMaxLength(rut, "RUT", 13);
    message = message + isEmpty(email1, "EMAIL");
    message = message + isMaxLength(email1, "EMAIL", 72);
    message = message + validateEmail(email1, "EMAIL");
    message = message + isEmpty(telefono1, "TELÉFONO OBLIGATORIO");
    message = message + isMaxLength(telefono1, "TELÉFONO OBLIGATORIO", 13);
    message = message + validatePhone(telefono1, "TELÉFONO OBLIGATORIO");
    message = message + isEmpty(telefono2, "TELÉFONO NO OBLIGATORIO");
    message = message + isMaxLength(telefono2, "TELÉFONO NO OBLIGATORIO", 13);
    message = message + validatePhone(telefono2, "TELÉFONO NO OBLIGATORIO");
    message = message + isEmpty(direccion1, "DIRECCIÓN");
    message = message + isMaxLength(direccion1, "DIRECCIÓN", 700);
    message = message + isEmpty(password1, "PASSWORD");
    message = message + isMaxLength(password1, "PASSWORD", 20);




    if (message.length > 0) {
        alert("ERROR: \n" + message);
    } else {
        document.getElementById(id).submit();
    }
}

function limpiar() {

    console.log("EJECUTANDO MÉTODO LIMPIAR.");
    //Validar si los campos existen (undefined o null)
    editElementText("txt-nombres", "");
    editElementText("txt-apellido-paterno", "");
    editElementText("txt-apellido-materno", "");
    editElementText("txt-rut", "");
    editElementText("txt-email1", "");
    editElementText("txt-telefono1", "");
    editElementText("txt-telefono2", "");
    editElementText("txt-direccion1", "");
    editElementText("txt-password1", "");
    editElementText("txt-password2", "");

    console.log("FINALIZANDO MÉTODO LIMPIAR.");
}

//OTRAS VALIDACIONES
function editElementText(elementName, value) {

    let element = document.getElementById(elementName);

    if (element != null) {
        element.value = value;
    } else {
        console.warn("El elemento " + elementName + " no se encuentra en el documento HTML");
    }
}

function validatePhone(phone, name) {
    let value = phone
    if (/^([0-9])*$/g.test(value)) {
        return "";
    } else {
        return "El campo " + name + " debe ser númerico.\n";
    }
}

function validateEmail(email, name) {

    let value = email
    if (/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(value)) {
        return "";
    } else {
        return "El campo " + name + " no es un correo electrónico válido\n";
    }
}