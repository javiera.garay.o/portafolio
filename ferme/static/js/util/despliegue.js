function mostrar(id) {
    if (id == "persona") {
        $("#persona").show();
        $("#empresa").hide();
        $("#selecciona").hide();
    }

    if (id == "empresa") {
        $("#persona").hide();
        $("#empresa").show();
        $("#selecciona").hide();
    }

    if (id == "selecciona") {
        $("#persona").hide();
        $("#empresa").hide();
        $("#selecciona").show();
    }
}