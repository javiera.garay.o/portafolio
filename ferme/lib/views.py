from django.http import HttpResponse
from django.template import Template, Context
from django.template.loader import get_template
from django.shortcuts import render
from persistencia.models import Comuna, Ciudad, Natural, Juridico
from persistencia.lib.dao import guardarPersona, guardarEmpresa



def home(request):
    return render(request, "home.html")

def registro_cliente(request):
    return render(request, "registro_cliente.html")    


def registro_cliente(request):
    print('REGISTRAR CLIENTE PERSONA')

    message_error = ''
    message_success = ''
    if request.method == 'POST': 

        nombres = request.POST['nombres']
        apellido_paterno = request.POST['apellido-paterno']
        apellido_materno = request.POST['apellido-materno']
        rut = request.POST['rut']
        email1 = request.POST['email1']
        telefono1 = request.POST['telefono1']
        telefono2 = request.POST['telefono2']
        direccion = request.POST['direccion1']
        password1 = request.POST['password1']        
        print('nombres: {0}'.format(nombres))
        print('apellido_paterno: {0}'.format(apellido_paterno))
        print('apellido_materno: {0}'.format(apellido_materno))
        print('rut: {0}'.format(rut))
        print('email: {0}'.format(email1))
        print('telefono: {0}'.format(telefono1))
        print('telefono: {0}'.format(telefono2))
        print('direccion1: {0}'.format(direccion))
        print('password1: {0}'.format(password1))
        

        clienteNatural = Natural(nombres=nombres,
            apellido_pat=apellido_paterno, apellido_mat=apellido_materno,
            rut=rut,email=email1, telefono1=telefono1, telefono2=telefono2, 
            direccion=direccion, password=password1)
        resultado = guardarPersona(clienteNatural)
        print('RESULTADO: {0}'.format(resultado))
        if resultado:
            message_success = 'DATOS GUARDADOS.'
        else:
            message_error = 'ERROR AL GUARDAR EL REGISTRO.'
    else:
        print('METODO NO SOPORTADO.')
    return render(request, "registro_cliente.html", 
        {'message_error': message_error, 'message_success': message_success}) 


def registro_cliente(request):
    print('REGISTRAR CLIENTE EMPRESA')

    message_error = ''
    message_success = ''
    if request.method == 'POST': 

        razon_social = request.POST['razonsocial']
        rol = request.POST['rol']
        email2 = request.POST['email2']
        telefono1 = request.POST['telefono3']
        telefono2 = request.POST['telefono4']
        direccion = request.POST['direccion2']
        password3 = request.POST['password3']        
        print('razon_social: {0}'.format(razon_social))
        print('rol: {0}'.format(rol))
        print('email: {0}'.format(email2))
        print('telefono: {0}'.format(telefono1))
        print('telefono: {0}'.format(telefono2))
        print('direccion: {0}'.format(direccion))
        print('password: {0}'.format(password3))
        

        clienteJuridico = Juridico(razon_social=razon_social,
            rol=rol,email=email2, telefono1=telefono1, telefono2=telefono2, 
            direccion=direccion, password=password3)
        resultado = guardarEmpresa(clienteJuridico)
        print('RESULTADO: {0}'.format(resultado))
        if resultado:
            message_success = 'DATOS GUARDADOS.'
        else:
            message_error = 'ERROR AL GUARDAR EL REGISTRO.'
    else:
        print('METODO NO SOPORTADO.')
    return render(request, "registro_cliente.html", 
        {'message_error': message_error, 'message_success': message_success})           
    