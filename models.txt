# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128, blank=True, null=True)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150, blank=True, null=True)
    first_name = models.CharField(max_length=150, blank=True, null=True)
    last_name = models.CharField(max_length=150, blank=True, null=True)
    email = models.CharField(max_length=254, blank=True, null=True)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class Boleta(models.Model):
    id_boleta = models.BigIntegerField(primary_key=True)
    fecha_boleta = models.DateField()
    neto = models.BigIntegerField()
    iva = models.BigIntegerField()
    total = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'boleta'


class Ciudad(models.Model):
    id_ciudad = models.BigIntegerField(primary_key=True)
    descripcion = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'ciudad'


class Cliente(models.Model):
    tipo_cliente_id_tipo_cli = models.ForeignKey('TipoCliente', models.DO_NOTHING, db_column='tipo_cliente_id_tipo_cli')
    id_cli = models.BigIntegerField(primary_key=True)
    usuario = models.CharField(max_length=30)
    contrasena_c = models.CharField(max_length=15)
    comuna_id_comuna = models.ForeignKey('Comuna', models.DO_NOTHING, db_column='comuna_id_comuna')

    class Meta:
        managed = False
        db_table = 'cliente'


class Comuna(models.Model):
    id_comuna = models.BigIntegerField(primary_key=True)
    ciudad_id_ciudad = models.ForeignKey(Ciudad, models.DO_NOTHING, db_column='ciudad_id_ciudad')
    descripcion = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'comuna'


class Despacho(models.Model):
    venta_id_venta = models.OneToOneField('Venta', models.DO_NOTHING, db_column='venta_id_venta', primary_key=True)
    id_despacho = models.BigIntegerField()
    venta_cliente_id_cli = models.ForeignKey('Venta', models.DO_NOTHING, db_column='venta_cliente_id_cli')
    venta_id_empl = models.ForeignKey('Venta', models.DO_NOTHING, db_column='venta_id_empl')
    venta_det_venta_id_det_venta = models.ForeignKey('Venta', models.DO_NOTHING, db_column='venta_det_venta_id_det_venta')
    fecha = models.DateField()
    venta_id_tipo_empl = models.ForeignKey('Venta', models.DO_NOTHING, db_column='venta_id_tipo_empl')
    venta_det_venta_id_prod = models.ForeignKey('Venta', models.DO_NOTHING, db_column='venta_det_venta_id_prod')
    venta_det_venta_id_tipo_prod = models.ForeignKey('Venta', models.DO_NOTHING, db_column='venta_det_venta_id_tipo_prod')

    class Meta:
        managed = False
        db_table = 'despacho'
        unique_together = (('venta_id_venta', 'venta_cliente_id_cli', 'venta_id_empl', 'venta_det_venta_id_det_venta', 'venta_id_tipo_empl', 'venta_det_venta_id_prod', 'venta_det_venta_id_tipo_prod', 'id_despacho'),)


class DetOrCompra(models.Model):
    id_det_oc = models.BigIntegerField(primary_key=True)
    cod_producto = models.BigIntegerField()
    descripcion = models.CharField(max_length=30)
    cantidad = models.BigIntegerField()
    or_compra_proveedor_id_prov = models.ForeignKey('OrCompra', models.DO_NOTHING, db_column='or_compra_proveedor_id_prov')
    or_compra_id_oc = models.ForeignKey('OrCompra', models.DO_NOTHING, db_column='or_compra_id_oc')
    producto_id_prod = models.ForeignKey('Producto', models.DO_NOTHING, db_column='producto_id_prod')
    pro_tipo_pro_id_tipo_prod = models.ForeignKey('Producto', models.DO_NOTHING, db_column='pro_tipo_pro_id_tipo_prod')

    class Meta:
        managed = False
        db_table = 'det_or_compra'
        unique_together = (('id_det_oc', 'or_compra_proveedor_id_prov', 'or_compra_id_oc', 'producto_id_prod', 'pro_tipo_pro_id_tipo_prod'),)


class DetVenta(models.Model):
    id_det_venta = models.BigIntegerField(primary_key=True)
    cantidad = models.BigIntegerField()
    total = models.BigIntegerField()
    producto_id_prod = models.ForeignKey('Producto', models.DO_NOTHING, db_column='producto_id_prod')
    pro_tipo_prod_id_tipo_prod = models.ForeignKey('Producto', models.DO_NOTHING, db_column='pro_tipo_prod_id_tipo_prod')

    class Meta:
        managed = False
        db_table = 'det_venta'
        unique_together = (('id_det_venta', 'producto_id_prod', 'pro_tipo_prod_id_tipo_prod'),)


class DetalleBoleta(models.Model):
    id_det_boleta = models.BigIntegerField(primary_key=True)
    boleta_id_boleta = models.ForeignKey(Boleta, models.DO_NOTHING, db_column='boleta_id_boleta')
    cod_producto = models.BigIntegerField()
    descripcion = models.CharField(max_length=30)
    cantidad = models.BigIntegerField()
    precio_unitario = models.BigIntegerField()
    valor = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'detalle_boleta'


class DetalleFactura(models.Model):
    id_det_fact = models.BigIntegerField(primary_key=True)
    factura_id_factura = models.ForeignKey('Factura', models.DO_NOTHING, db_column='factura_id_factura')
    cod_producto = models.BigIntegerField()
    descripcion = models.CharField(max_length=30)
    cantidad = models.BigIntegerField()
    precio_unitario = models.BigIntegerField()
    valor = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'detalle_factura'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200, blank=True, null=True)
    action_flag = models.IntegerField()
    change_message = models.TextField(blank=True, null=True)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100, blank=True, null=True)
    model = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255, blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField(blank=True, null=True)
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class Documento(models.Model):
    venta_id_venta = models.OneToOneField('Venta', models.DO_NOTHING, db_column='venta_id_venta', primary_key=True)
    venta_cliente_id_cli = models.ForeignKey('Venta', models.DO_NOTHING, db_column='venta_cliente_id_cli')
    venta_empleado_id_empl = models.ForeignKey('Venta', models.DO_NOTHING, db_column='venta_empleado_id_empl')
    id_doc = models.BigIntegerField()
    venta_det_venta_id_det_venta = models.ForeignKey('Venta', models.DO_NOTHING, db_column='venta_det_venta_id_det_venta')
    descripcion = models.CharField(max_length=30)
    venta_empleado_id_tipo_empl = models.ForeignKey('Venta', models.DO_NOTHING, db_column='venta_empleado_id_tipo_empl')
    venta_det_venta_id_prod = models.ForeignKey('Venta', models.DO_NOTHING, db_column='venta_det_venta_id_prod')
    venta_det_venta_id_tipo_prod = models.ForeignKey('Venta', models.DO_NOTHING, db_column='venta_det_venta_id_tipo_prod')
    boleta_id_boleta = models.ForeignKey(Boleta, models.DO_NOTHING, db_column='boleta_id_boleta', blank=True, null=True)
    factura_id_factura = models.ForeignKey('Factura', models.DO_NOTHING, db_column='factura_id_factura', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'documento'
        unique_together = (('venta_id_venta', 'venta_cliente_id_cli', 'venta_empleado_id_empl', 'venta_det_venta_id_det_venta', 'venta_empleado_id_tipo_empl', 'venta_det_venta_id_prod', 'venta_det_venta_id_tipo_prod', 'id_doc'),)


class Empleado(models.Model):
    id_empl = models.BigIntegerField(primary_key=True)
    sucursal_id_sucursal = models.ForeignKey('Sucursal', models.DO_NOTHING, db_column='sucursal_id_sucursal')
    rut = models.CharField(max_length=15)
    nombres = models.CharField(max_length=30)
    apellido_pat = models.CharField(max_length=30)
    apellido_mat = models.CharField(max_length=30)
    numero1 = models.CharField(max_length=15)
    numero2 = models.CharField(max_length=15, blank=True, null=True)
    email = models.CharField(max_length=30)
    contrasena_e = models.CharField(max_length=15)
    tipo_empleado_id_tipo_empl = models.ForeignKey('TipoEmpleado', models.DO_NOTHING, db_column='tipo_empleado_id_tipo_empl')

    class Meta:
        managed = False
        db_table = 'empleado'
        unique_together = (('id_empl', 'tipo_empleado_id_tipo_empl'),)


class EstadoOrCompra(models.Model):
    id_estado_oc = models.BigIntegerField(primary_key=True)
    descripcion = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'estado_or_compra'


class EstadoVenta(models.Model):
    id_est_venta = models.BigIntegerField(primary_key=True)
    descripcion = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'estado_venta'


class Factura(models.Model):
    id_factura = models.BigIntegerField(primary_key=True)
    fecha_factura = models.DateField()
    neto = models.BigIntegerField()
    iva = models.BigIntegerField()
    total = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'factura'


class Informe(models.Model):
    empleado_id_empl = models.ForeignKey(Empleado, models.DO_NOTHING, db_column='empleado_id_empl')
    id_informe = models.BigIntegerField(primary_key=True)
    descripcion = models.CharField(max_length=30)
    empleado_id_tipo_empl = models.ForeignKey(Empleado, models.DO_NOTHING, db_column='empleado_id_tipo_empl')

    class Meta:
        managed = False
        db_table = 'informe'


class Juridico(models.Model):
    id_juridico = models.BigIntegerField(primary_key=True)
    rol = models.CharField(max_length=15)
    razon_social = models.CharField(max_length=30)
    direccion = models.CharField(max_length=50)
    telefono1 = models.CharField(max_length=15)
    telefono2 = models.CharField(max_length=15, blank=True, null=True)
    email = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'juridico'


class MarcaProducto(models.Model):
    id_marca_prod = models.BigIntegerField(primary_key=True)
    descripcion = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'marca_producto'


class Natural(models.Model):
    id_natural = models.AutoField(db_column='ID_NATURAL', primary_key=True)
    rut = models.CharField(db_column='RUT', max_length=15)
    nombres = models.CharField(db_column='NOMBRES', max_length=30)
    apellido_pat = models.CharField(db_column='APELLIDO_PAT', max_length=30)
    apellido_mat = models.CharField(db_column='APELLIDO_MAT', max_length=30)
    direccion = models.CharField(db_column='DIRECCION', max_length=50)
    telefono1 = models.CharField(db_column='TELEFONO1', max_length=15)
    telefono2 = models.CharField(db_column='TELEFONO2', max_length=15)
    email = models.CharField(db_column='EMAIL', max_length=30)

    def __str__(self):
        return 'ID: {0}, NOMBRES {1}, APELLIDO_PAT: {2}, APELLIDO_MAT: {3}'.format(
            self.id, self.nombres, self.apellido_pat, self.apellido_mat)


    class Meta:
        managed = False
        db_table = 'natural'


class OrCompra(models.Model):
    proveedor_id_prov = models.OneToOneField('Proveedor', models.DO_NOTHING, db_column='proveedor_id_prov', primary_key=True)
    id_oc = models.BigIntegerField()
    fecha = models.DateField()
    empleado_id_empl = models.ForeignKey(Empleado, models.DO_NOTHING, db_column='empleado_id_empl')
    emp_tipo_emp_id_tipo_empl = models.ForeignKey(Empleado, models.DO_NOTHING, db_column='emp_tipo_emp_id_tipo_empl')
    estado_or_compra_id_estado_oc = models.ForeignKey(EstadoOrCompra, models.DO_NOTHING, db_column='estado_or_compra_id_estado_oc')

    class Meta:
        managed = False
        db_table = 'or_compra'
        unique_together = (('proveedor_id_prov', 'id_oc'),)


class Pago(models.Model):
    venta_id_venta = models.OneToOneField('Venta', models.DO_NOTHING, db_column='venta_id_venta', primary_key=True)
    venta_cliente_id_cli = models.ForeignKey('Venta', models.DO_NOTHING, db_column='venta_cliente_id_cli')
    venta_empleado_id_empl = models.ForeignKey('Venta', models.DO_NOTHING, db_column='venta_empleado_id_empl')
    id_pago = models.BigIntegerField()
    tipo_pago_id_tipo_pago = models.ForeignKey('TipoPago', models.DO_NOTHING, db_column='tipo_pago_id_tipo_pago')
    venta_det_venta_id_det_venta = models.ForeignKey('Venta', models.DO_NOTHING, db_column='venta_det_venta_id_det_venta')
    venta_empleado_id_tipo_empl = models.ForeignKey('Venta', models.DO_NOTHING, db_column='venta_empleado_id_tipo_empl')
    venta_det_venta_id_prod = models.ForeignKey('Venta', models.DO_NOTHING, db_column='venta_det_venta_id_prod')
    venta_det_venta_id_tipo_prod = models.ForeignKey('Venta', models.DO_NOTHING, db_column='venta_det_venta_id_tipo_prod')
    fecha = models.DateField()

    class Meta:
        managed = False
        db_table = 'pago'
        unique_together = (('venta_id_venta', 'venta_cliente_id_cli', 'venta_empleado_id_empl', 'venta_det_venta_id_det_venta', 'venta_empleado_id_tipo_empl', 'venta_det_venta_id_prod', 'venta_det_venta_id_tipo_prod', 'id_pago', 'tipo_pago_id_tipo_pago'),)


class Producto(models.Model):
    id_prod = models.BigIntegerField(primary_key=True)
    tipo_prod_id_tipo_prod = models.ForeignKey('TipoProd', models.DO_NOTHING, db_column='tipo_prod_id_tipo_prod')
    marca_producto_id_marca_prod = models.ForeignKey(MarcaProducto, models.DO_NOTHING, db_column='marca_producto_id_marca_prod')
    descripcion = models.CharField(max_length=30)
    stock = models.BigIntegerField()
    precio_unitario = models.BigIntegerField()
    imagen = models.BinaryField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'producto'
        unique_together = (('id_prod', 'tipo_prod_id_tipo_prod'),)


class Proveedor(models.Model):
    id_prov = models.BigIntegerField(primary_key=True)
    rut = models.CharField(max_length=15)
    nombres = models.CharField(max_length=30)
    apellido_pat = models.CharField(max_length=30)
    apellido_mat = models.CharField(max_length=30)
    rol = models.CharField(max_length=15)
    razon_social = models.CharField(max_length=30)
    numero1 = models.CharField(max_length=15)
    numero2 = models.CharField(max_length=15, blank=True, null=True)
    email = models.CharField(max_length=30)
    direccion = models.CharField(max_length=30)
    contrasena_p = models.CharField(max_length=15)
    comuna_id_comuna = models.ForeignKey(Comuna, models.DO_NOTHING, db_column='comuna_id_comuna')

    class Meta:
        managed = False
        db_table = 'proveedor'


class Sucursal(models.Model):
    id_sucursal = models.BigIntegerField(primary_key=True)
    comuna_id_comuna = models.ForeignKey(Comuna, models.DO_NOTHING, db_column='comuna_id_comuna')
    descripcion = models.CharField(max_length=30)
    direccion = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'sucursal'


class TipoCliente(models.Model):
    id_tipo_cli = models.BigIntegerField(primary_key=True)
    natural_id_natural = models.ForeignKey(Natural, models.DO_NOTHING, db_column='natural_id_natural', blank=True, null=True)
    juridico_id_juridico = models.ForeignKey(Juridico, models.DO_NOTHING, db_column='juridico_id_juridico', blank=True, null=True)
    descripcion = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'tipo_cliente'


class TipoEmpleado(models.Model):
    id_tipo_empl = models.BigIntegerField(primary_key=True)
    descripcion = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'tipo_empleado'


class TipoPago(models.Model):
    id_tipo_pago = models.BigIntegerField(primary_key=True)
    descripcion = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'tipo_pago'


class TipoProd(models.Model):
    id_tipo_prod = models.BigIntegerField(primary_key=True)
    descripcion = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'tipo_prod'


class Venta(models.Model):
    id_venta = models.BigIntegerField(primary_key=True)
    cliente_id_cli = models.ForeignKey(Cliente, models.DO_NOTHING, db_column='cliente_id_cli')
    empleado_id_empl = models.ForeignKey(Empleado, models.DO_NOTHING, db_column='empleado_id_empl')
    det_venta_id_det_venta = models.ForeignKey(DetVenta, models.DO_NOTHING, db_column='det_venta_id_det_venta')
    fecha_venta = models.DateField()
    valor_neto = models.BigIntegerField()
    empleado_id_tipo_empl = models.ForeignKey(Empleado, models.DO_NOTHING, db_column='empleado_id_tipo_empl')
    det_venta_id_prod = models.ForeignKey(DetVenta, models.DO_NOTHING, db_column='det_venta_id_prod')
    det_venta_id_tipo_prod = models.ForeignKey(DetVenta, models.DO_NOTHING, db_column='det_venta_id_tipo_prod')
    estado_venta_id_est_venta = models.ForeignKey(EstadoVenta, models.DO_NOTHING, db_column='estado_venta_id_est_venta')

    class Meta:
        managed = False
        db_table = 'venta'
        unique_together = (('id_venta', 'cliente_id_cli', 'empleado_id_empl', 'empleado_id_tipo_empl', 'det_venta_id_det_venta', 'det_venta_id_prod', 'det_venta_id_tipo_prod'),)
