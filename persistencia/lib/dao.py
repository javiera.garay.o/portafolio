from persistencia.models import Natural, Juridico, Comuna, Ciudad
from django.core.exceptions import ObjectDoesNotExist, EmptyResultSet
from django.db import IntegrityError, Error
from datetime import datetime

def guardarPersona(Natural):
    try:
        Natural.save()
        print('Datos cliente persona Guardado')
        print(Natural)
        return True
    except IntegrityError as e:
        print(e)
        print('ERROR: Datos cliente persona no fue almecenado.')
        return False 

def guardarEmpresa(Juridico):
    try:
        Juridico.save()
        print('Datos cliente empresa Guardado')
        print(Juridico)
        return True
    except IntegrityError as e:
        print(e)
        print('ERROR: Datos cliente empresa no fue almecenado.')
        return False 

def guardarComuna(Comuna):
    try:
        Comuna.save()
        print('Comuna fue almacenado')
        print(Comuna)
        return True
    except IntegrityError as e:
        print(e)
        print('ERROR: Comuna no fue almacenado')
        return False  

def guardarComuna(Ciudad):
    try:
        Ciudad.save()
        print('Ciudad fue almacenado')
        print(Ciudad)
        return True
    except IntegrityError as e:
        print(e)
        print('ERROR: Ciudad no fue almacenado')
        return False                                         