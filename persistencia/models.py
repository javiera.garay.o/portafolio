# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models

class Natural(models.Model):
    id_natural = models.AutoField(db_column='ID_NATURAL', primary_key=True)
    rut = models.CharField(db_column='RUT', max_length=15)
    nombres = models.CharField(db_column='NOMBRES', max_length=30)
    apellido_pat = models.CharField(db_column='APELLIDO_PAT', max_length=30)
    apellido_mat = models.CharField(db_column='APELLIDO_MAT', max_length=30)
    direccion = models.CharField(db_column='DIRECCION', max_length=50)
    telefono1 = models.CharField(db_column='TELEFONO1', max_length=15)
    telefono2 = models.CharField(db_column='TELEFONO2', max_length=15)
    email = models.CharField(db_column='EMAIL', max_length=30)
    password = models.CharField(db_column='PASSWORD', max_length=20)

    def __str__(self):
        return 'ID_NATURAL: {0}, NOMBRES: {1}, APELLIDO_PAT: {2}, APELLIDO_MAT: {3}'.format(
            self.id_natural, self.nombres, self.apellido_pat, self.apellido_mat)


    class Meta:
        managed = False
        db_table = 'NATURAL'

class Juridico(models.Model):
    id_juridico =  models.AutoField(db_column='ID_JURIDICO', primary_key=True)
    rol = models.CharField(db_column='ROL', max_length=15)
    razon_social = models.CharField(db_column='RAZON_SOCIAL', max_length=30)
    direccion = models.CharField(db_column='DIRECCION', max_length=50)
    telefono1 = models.CharField(db_column='TELEFONO1', max_length=15)
    telefono2 = models.CharField(db_column='TELEFONO2', max_length=15)
    email = models.CharField(db_column='EMAIL', max_length=30)
    password = models.CharField(db_column='PASSWORD', max_length=20)

    def __str__(self):
        return 'ID_JURIDICO: {0}, RAZON_SOCIAL: {1}, ROL: {2}'.format(
            self.id_juridico, self.razon_social, self.rol)


    class Meta:
        managed = False
        db_table = 'JURIDICO' 

ciudad_id_ciudad = [
    (1, 'SANTIAGO')
]        

class Ciudad(models.Model):
    id_ciudad = models.BigIntegerField(db_column='CIUDAD', primary_key=True, choices=ciudad_id_ciudad)
    descripcion = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'CIUDAD'

Comuna_id_comuna = [
    (1,	'Cerrillos'),
    (2,	'Cerro Navia'),
    (3,	'Conchalí'),
    (4,	'El Bosque'),
    (5,	'Estación Central'),
    (6,	'Huechuraba'),
    (7,	'Independencia'),
    (8,	'La Cisterna'),
    (9,	'La Florida'),
    (10,'La Granja'),
    (11,'La Pintana'),
    (12,'La Reina'),
    (13,'Las Condes'),
    (14,'Lo Barnechea'),
    (15,'Lo Espejo'),
    (16,'Lo Prado'),
    (17,'Macul'),
    (18,'Maipú'),
    (19,'Ñuñoa'),
    (20,'Padre Hurtado'),
    (21,'Pedro Aguirre Cerda'),
    (22,'Peñalolén'),
    (23,'Pirque'),
    (24,'Providencia'),
    (25,'Pudahuel'),
    (26,'Puente Alto'),
    (27,'Quilicura'),
    (28,'Quinta Normal'),
    (29,'Recoleta'),
    (30,'Renca'),
    (31,'San Bernardo'),
    (32,'San Joaquín'),
    (33,'San José de Maipo'),
    (34,'San Miguel'),
    (35,'San Ramón'),
    (36,'Santiago')    
]          

class Comuna(models.Model):
    id_comuna = models.BigIntegerField(db_column='COMUNA', primary_key=True, choices=Comuna_id_comuna)
    ciudad_id_ciudad = models.ForeignKey(Ciudad, models.DO_NOTHING, db_column='CIUDAD_ID_CIUDAD')
    descripcion = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'COMUNA'                   
